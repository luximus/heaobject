import unittest
from heaobject import dataset, registry, folder
import json


class TestFromJSONAndDict(unittest.TestCase):

    def test_from_dict(self):
        expected = dataset.Dataset()
        expected.name = 'foo'
        expected.description = 'bar'
        jsn = {
            'type': dataset.Dataset.get_type_name(),
            'name': 'foo',
            'description': 'bar'
        }
        ds = dataset.Dataset()
        ds.from_dict(jsn)
        self.assertEqual(expected.to_dict(), ds.to_dict())

    def test_from_dict_with_nested(self):
        expected = registry.Component()
        expected.name = 'foo'
        expected.description = 'bar'
        resource = registry.Resource()
        resource.resource_type_name = folder.Folder.get_type_name()
        expected.resources = [resource]
        jsn = {
            'type': registry.Component.get_type_name(),
            'name': 'foo',
            'description': 'bar',
            'resources': [
                {
                    'type': registry.Resource.get_type_name(),
                    'resource_type_name': folder.Folder.get_type_name()
                }
            ]
        }
        jsn_str = json.dumps(jsn)
        c = registry.Component()
        c.json_loads(jsn_str)
        self.maxDiff = None
        self.assertDictEqual(expected.to_dict(), c.to_dict())
