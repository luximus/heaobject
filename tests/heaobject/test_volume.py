import unittest
from heaobject import volume


class NotStringable:
    """
    A class that cannot be converted into a string.
    """
    def __str__(self):
        return


class TestVolumeClass(unittest.TestCase):
    test_volume = volume.Volume()

    def test_file_system_id_failure(self):
        with self.assertRaises(
                TypeError,
                msg='Setting file_system_id successful with object that cannot be converted to a string'):
            self.test_volume.file_system_id = NotStringable()

    def test_credentials_id_failure(self):
        with self.assertRaises(
                TypeError,
                msg='Setting credentials_id successful with object that cannot be converted to a string'):
            self.test_volume.credentials_id = NotStringable()

    def test_set_file_system_id(self):
        try:
            self.test_volume.file_system_id = 'Documents'
        except TypeError:
            self.fail(msg='Setting file_system_id unsuccessful with object that can be converted to a string')

    def test_set_credentials_id(self):
        try:
            self.test_volume.credentials_id = 'Luximus'
        except TypeError:
            self.fail(msg='Setting credentials_id unsuccessful with object that can be converted to a string')
