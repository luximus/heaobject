import unittest
from heaobject import dataset


class TestJSONDumps(unittest.TestCase):

    def test_json_dumps_method(self):
        expected = dataset.Dataset()
        expected.name = 'foo'
        expected.description = 'bar'
        str_ = expected.json_dumps()
        actual = dataset.Dataset()
        actual.json_loads(str_)
        self.assertEqual(expected.to_dict(), actual.to_dict())
