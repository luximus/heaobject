import unittest
from heaobject import registry, folder


class TestGetAttributes(unittest.TestCase):

    def test_get_folder_attributes(self):
        expected = {'source', 'version', 'name', 'display_name', 'description', 'owner',
                    'created', 'modified', 'derived_by', 'derived_from', 'invites',
                    'shares', 'type', 'id', 'volume_id'}
        self.assertEqual(expected, set(folder.Folder().get_attributes()))

    def test_get_registry_attributes(self):
        expected = {'source', 'version', 'name', 'display_name', 'description', 'owner',
                    'created', 'modified', 'derived_by', 'derived_from', 'invites',
                    'shares', 'type', 'resources', 'base_url', 'id'}
        self.assertEqual(expected, set(registry.Component().get_attributes()))
