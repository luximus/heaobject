import unittest
from heaobject import dataset
import json


class TestJSONLoadsAndDict(unittest.TestCase):

    def test_json_loads(self):
        expected = dataset.Dataset()
        expected.name = 'foo'
        expected.description = 'bar'
        jsn = {
            'type': dataset.Dataset.__module__ + '.' + dataset.Dataset.__name__,
            'name': 'foo',
            'description': 'bar'
        }
        ds = dataset.Dataset()
        ds.json_loads(json.dumps(jsn))
        self.assertEqual(expected.to_dict(), ds.to_dict())


