import unittest
from heaobject import dataset, registry, folder, user


class TestToJSONAndDict(unittest.TestCase):

    def test_to_dict(self):
        actual = dataset.Dataset()
        actual.name = 'foo'
        actual.description = 'bar'
        expected = {
            'type': dataset.Dataset.get_type_name(),
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'description': 'bar',
            'display_name': None,
            'id': None,
            'invites': [],
            'modified': None,
            'name': 'foo',
            'owner': user.NONE_USER,
            'shares': [],
            'source': None,
            'version': None
        }
        self.assertDictEqual(expected, actual.to_dict())

    def test_to_dict_with_nested(self):
        actual = registry.Component()
        actual.name = 'foo'
        actual.description = 'bar'
        resource = registry.Resource()
        resource.resource_type_name = folder.Folder.get_type_name()
        resource.base_path = '/'
        actual.resources = [resource]
        self.maxDiff = None
        expected = {
            'type': registry.Component.get_type_name(),
            'name': 'foo',
            'description': 'bar',
            'created': None,
            'derived_by': None,
            'derived_from': [],
            'display_name': None,
            'id': None,
            'invites': [],
            'modified': None,
            'owner': user.NONE_USER,
            'shares': [],
            'source': None,
            'version': None,
            'base_url': None,
            'resources': [
                {
                    'type': registry.Resource.get_type_name(),
                    'resource_type_name': folder.Folder.get_type_name(),
                    'base_path': '/'
                }
            ]
        }
        self.assertDictEqual(expected, actual.to_dict())
