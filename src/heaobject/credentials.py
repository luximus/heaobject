from typing import Optional
import abc
from heaobject import root


class Credentials(root.AbstractDesktopObject, abc.ABC):
    """
    Stores usernames and passwords, and other kinds of credentials for use by other HEA objects.
    """
    pass


class UsernamePasswordCredentials(Credentials):
    """
    Stores usernames and passwords for use by other HEA objects.
    """
    def __init__(self):
        super().__init__()
        self.__username: Optional[str] = None
        self.__password: Optional[str] = None

    @property  # type: ignore
    def username(self) -> Optional[str]:
        """
        A username.
        """
        return self.__username

    @username.setter  # type: ignore
    def username(self, username_: Optional[str]) -> None:
        self.__username = str(username_) if username_ else None

    @property  # type: ignore
    def password(self) -> Optional[str]:
        """
        A password.
        """
        return self.__password

    @password.setter  # type: ignore
    def password(self, password_: Optional[str]) -> None:
        self.__password = str(password_) if password_ else None
