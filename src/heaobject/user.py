"""
Defines the following reserved usernames:

NONE_USER: 'system|none' -- for files that are installed by default and are not owned by any particular user.
ALL_USERS: 'system|all' -- for use in sharing a file with all users.

Furthermore, all usernames beginning with 'system|' are reserved for system use.
"""

NONE_USER = 'system|none'
ALL_USERS = 'system|all'
