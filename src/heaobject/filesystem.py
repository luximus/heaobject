import abc
from heaobject import root


class FileSystem(root.AbstractDesktopObject, abc.ABC):
    """
    Represents a filesystem, which controls how data is stored and retrieved. In HEA, all filesystems are either
    databases or network file storage.
    """
    pass


class MongoDBFileSystem(FileSystem):
    """
    The default storage for HEA objects.
    """
    pass