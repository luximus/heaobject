from .root import AbstractMemberObject


class Source(AbstractMemberObject):
    """
    An HEAObject's source.
    """
    pass