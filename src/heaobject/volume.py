from typing import Optional
from heaobject import root


class Volume(root.AbstractDesktopObject):
    """
    A single accessible storage area that stores a single filesystem. Some volumes may require providing credentials in
    order to access them.
    """
    def __init__(self):
        super().__init__()
        self.__file_system_id: Optional[str] = None
        self.__credentials_id: Optional[str] = None

    @property  # type: ignore
    def file_system_id(self) -> Optional[str]:
        """
        The id of this volume's filesystem (a FileSystem object).
        """
        return self.__file_system_id

    @file_system_id.setter  # type: ignore
    def file_system_id(self, file_system_id_: Optional[str]) -> None:
        self.__file_system_id = str(file_system_id_) if file_system_id_ else None

    @property  # type: ignore
    def credentials_id(self) -> Optional[str]:
        """
        The id of this volume's credentials required for access (a Credentials object).
        """
        return self.__credentials_id

    @credentials_id.setter  # type: ignore
    def credentials_id(self, credentials_id_: Optional[str]) -> None:
        self.__credentials_id = str(credentials_id_) if credentials_id_ else None
